package com.ADSI1836648.Bike.domain.enumeration;

public enum Gender {
    MALE, FEMALE, OTHER
}
