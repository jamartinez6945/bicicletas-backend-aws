package com.ADSI1836648.Bike.repository;

import com.ADSI1836648.Bike.domain.Rols;
import org.springframework.data.repository.CrudRepository;

public interface RolRepository extends CrudRepository<Rols, Integer> {
}
