package com.ADSI1836648.Bike.web.rest;

import com.ADSI1836648.Bike.domain.Users;
import com.ADSI1836648.Bike.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserResource {

    @Autowired
    IUserService userService;

    @PostMapping("/users")
    public Users save(@RequestBody Users user){
        return userService.save(user);
    }

    @GetMapping("/users")
    public Iterable<Users> getUsers(){
        return userService.getAll();
    }
}
